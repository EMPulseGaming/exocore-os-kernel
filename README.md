# ![logo](http://lycus.org/Lycus.png)

## Introduction

 ExoCore is an operating system kernel written in the C programming language    
	(C11 with GNU extensions, specifically meant to be built with Clang). It is      
	a so-called exokernel; it only provides the bare minimum of abstraction needed      
	around the hardware. This allows for most aspects of real operating systems to      
	be implemented in user space.      
	       
   ExoCore is primarily a reference project. It is not intended to be a kernel       
	for real world usage; rather, to be a learning project for the developers and      
	anyone else interested in kernel development.      

   If you wish to contribute ideas or code please visit our site linked below or      
       make pull requests to our [BitBucket repository](https://goo.gl/vFdrLh).      
       
   For further information on the The Lycus Foundation,        
	   please visit our project website at: [FACEPAGE](http://lycus.org/).      

## INSTALL
 
   Detailed installation guides are available in the wiki for      
       [ExoCore 1.0 documentation](http://exocore.readthedocs.io/en/latest/)      

## INFORMATION:
     
   For project Documentation, see:         
        * [LICENSE](LICENSE.md)    
          - Licensing and copyright information.            
        * [COMPATIBILITY](misc/COMPATIBILITY)      
          - Supported compilers, architectures, etc.             
        * [INSTALL](misc/INSTALL)       
           - Instructions on building and installing ExoCore.     
        * [AUTHORS](misc/AUTHORS)       
           - Names and contact information for ExoCore developers.    
        * [THANKS](misc/THANKS)     
           - Credits and appreciation where due.      
        * [STYLE](https://bitbucket.org/EMPulseGaming/exocore)     
           - Coding style rules (only relevant if you're a contributor).     
        
   See also the documentation in the '[docs](https://goo.gl/s1rVCw)' directory.       
       
## Links 
       
You can reach the ExoCore community in several ways:          
       
   * IRC channels     
      - #lycus @ irc.oftc.net      
   * Mailing lists       
      - lycus-announce: http://groups.google.com/group/lycus-announce     
      - lycus-discuss: http://groups.google.com/group/lycus-discuss       
      - lycus-develop: http://groups.google.com/group/lycus-develop         
        
 [homePage](The Lycus Foundation)     
 [Blogs](http://blog.lycus.org/)	     